# django
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# django contrib
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm

# local
from .forms import RegistrationForm


def registration_view(request):
    form = RegistrationForm(request.POST or None)
    if form.is_valid():
        user = form.save()
        return HttpResponseRedirect(reverse('registration_done'))
    return render(request, 'registration/registration.html', {
        'form': form,
    })


def registration_done_view(request):
    return render(request, 'registration/registration_done.html')

