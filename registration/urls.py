from django.conf.urls import patterns, include, url


urlpatterns = patterns('registration.views',
    url(r'^registration/$', 'registration_view', name='registration'),
    url(r'^registration/done/$', 'registration_done_view', name='registration_done'),
)