# django
from django import forms

# django contrib
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm

# django contrib
from django.contrib.auth.decorators import login_required


class RegistrationForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'email', 'password1', 'password2')