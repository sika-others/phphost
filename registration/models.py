# coding=utf-8
# django
from django.db import models

# django contrib
from django.contrib.auth.models import User


class Profile(models.Model):
    user = models.OneToOneField(User)

    def __unicode__(self):
        return u'%s' % self.user.username
