from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from django.views.generic import RedirectView
from django.conf import settings


admin.autodiscover()

urlpatterns = patterns('',
    url(r'^', include('phphost.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^accounts/profile/', RedirectView.as_view(url="/dashboard")),
    url(r'^register/', RedirectView.as_view(url="/accounts/register/")),
    url(r'^', include('login.urls')),
    url(r'^', include('password_reset.urls')),
    url(r'^', include('registration.urls')),

)

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
