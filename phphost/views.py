# django
from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

# django contrib
from django.contrib.auth.decorators import login_required

# local
from .models import Hosting, Server
from .forms import HostingForm
from .utils import sentry_log

def message_write(request, message):
    if 'messages' in request.session:
        request.session['messages'].append(message)
    else:
        request.session['messages'] = [message]
    request.session.save()


def message_get_all(request):
    messages = request.session.get('messages', [])
    request.session['messages'] = []
    return messages


@login_required
def dashboard_home_view(request):
    return render(request, 'phphost/dashboard/home.html')


@login_required
def dashboard_hosting_view(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: alert = request.GET["alert"]
    except KeyError: alert = ""
    return render(request, 'phphost/dashboard/hosting.html', {
        "hosting": hosting,
        "alert": alert,
        'messages': message_get_all(request),
    })


def dashboard_hosting_add_view(request):
    form = HostingForm(request.POST or None)
    if form.is_valid():
        obj = form.save(commit=False)
        obj.user = request.user
        obj.server = Server.objects.first()
        obj.save()
        s, rn = obj.shm_hosting_create()
        if s:
            message_write(request, ('success', 'Hosting byl vytvoren'))
        else:
            message_write(request, ('error', 'Nastala chyba #%s. Kontaktujte prosim podporu na <a href="mailto:support@phphost.cz">support@phphost.cz</a>' % rn))
            sentry_log('create apache host failed')
        s, rn = obj.shm_mysql_user_create()
        if s:
            message_write(request, ('success', 'MySQL uzivatel byl vytvoren'))
        else:
            message_write(request, ('error', 'Nastala chyba #%s. Kontaktujte prosim podporu na <a href="mailto:support@phphost.cz">support@phphost.cz</a>' % rn))
            sentry_log('create mysql user failed')
        return HttpResponseRedirect((reverse("phphost.dashboard.hosting", args=[obj.domain, ])))
    return render(request, 'phphost/dashboard/add_hosting.html', {
        "form": form,
    })


@login_required
def set_ftp_passwd_process(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: passwd = request.POST["passwd"]
    except KeyError: passwd = None
    if passwd:
        s, rn = hosting.shm_ftp_setpassword(passwd)
        if s:
            message_write(request, ('success', 'Heslo bylo zmeneno'))
        else:
            message_write(request, ('error', 'Nastala chyba #%s. Kontaktujte prosim podporu na <a href="mailto:support@phphost.cz">support@phphost.cz</a>' % rn))
            sentry_log('set ftp password failed')
    return HttpResponseRedirect(reverse("phphost.dashboard.hosting", args=[hosting.domain, ]))


@login_required
def set_mysql_passwd_process(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: passwd = request.POST["passwd"]
    except KeyError: passwd = None
    if passwd:
        s, rn = hosting.shm_mysql_user_setpassword(passwd)
        if s:
            message_write(request, ('success', 'Heslo bylo zmeneno'))
        else:
            message_write(request, ('error', 'Nastala chyba #%s. Kontaktujte prosim podporu na <a href="mailto:support@phphost.cz">support@phphost.cz</a>' % rn))
            sentry_log('set mysql password failed')
    return HttpResponseRedirect(reverse("phphost.dashboard.hosting", args=[hosting.domain, ]))


@login_required
def create_mysql_db_process(request, domain):
    hosting = get_object_or_404(Hosting, domain=domain)
    try: suffix = request.POST["suffix"]
    except KeyError: suffix = None
    if suffix and len(suffix) < 8:
        s, rn = hosting.shm_mysql_db_create(suffix)
        if s:
            message_write(request, ('success', 'Databaze byla vytvorena'))
        else:
            message_write(request, ('error', 'Nastala chyba #%s. Kontaktujte prosim podporu na <a href="mailto:support@phphost.cz">support@phphost.cz</a>' % rn))
            sentry_log('create mysql db failed')
    return HttpResponseRedirect(reverse("phphost.dashboard.hosting", args=[hosting.domain, ]))
