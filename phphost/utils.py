# django
from django.conf import settings

# lib
from raven import Client


def sentry_log(name, extra={}, level=None):
    if not settings.SENTRY_DSN:
        return
    client = Client(settings.SENTRY_DSN)
    data = None
    if level:
        data = {
            'level': level,
        }
    client.captureMessage(name, extra=extra, data=data)
