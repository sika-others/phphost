# django
from django import forms

# local
from .models import Hosting


class HostingForm(forms.ModelForm):
    class Meta:
        model = Hosting
        fields = ["domain", ]