# coding=utf-8
# python
import urllib
import string
import random

# lib
import requests

# django
from django.db import models

# django contrib
from django.contrib.auth.models import User


def get_random_string(length, chars=string.ascii_lowercase+string.digits):
    return "".join([random.choice(chars) for i in xrange(length)])


class Server(models.Model):
    domain = models.CharField(max_length=64, unique=True)
    ip = models.IPAddressField()
    phm_origin = models.CharField(max_length=64)

    def __unicode__(self):
        return u'%s' % self.domain


class Hosting(models.Model):
    user = models.ForeignKey(User)
    server = models.ForeignKey(Server)

    domain = models.CharField(max_length=255, unique=True)
    created_at = models.DateField(auto_now_add=True)

    def get_ftp_server(self):
        return self.server.domain

    def get_ftp_user(self):
        return 'user%s' % self.id

    def get_php_my_admin_url(self):
        return 'http://pma.%s' % self.server.domain

    def get_mysql_server(self):
        return 'localhost'

    def get_mysql_user(self):
        return 'mysql%s' % self.id

    def _shm(self, path, attrs):
        res = requests.get('%s%s?%s' % (self.server.phm_origin, path, urllib.urlencode(attrs))).text
        print self.server.phm_origin, path, res
        status, status_code = res.split()
        return True if status == 'OK' else False, int(status_code)

    def shm_hosting_create(self):
        return self._shm('/hosting/create/', {
            'id': self.id,
            'server_name': self.domain,
            'server_aliases': 'www.%s' % self.domain,
        })

    def shm_mysql_user_create(self):
        return self._shm('/mysql/user/create/', {
            'user': self.get_mysql_user(),
            'passwd': get_random_string(10),
        })

    def shm_mysql_user_setpassword(self, passwd):
        return self._shm('/mysql/user/setpassword/', {
            'user': self.get_mysql_user(),
            'passwd': passwd,
        })

    def shm_mysql_db_create(self, suffix):
        return self._shm('/mysql/db/create/', {
            'name': '%s_%s' % (self.get_mysql_user(), suffix),
        })

    def shm_ftp_create(self):
        return self._shm('/ftp/create/', {
            'username': self.get_ftp_user(),
            'server_name': self.domain,
        })

    def shm_ftp_setpassword(self, passwd):
        return self._shm('/ftp/setpassword/', {
            'username': self.get_ftp_user(),
            'passwd': passwd,
        })

    def __unicode__(self):
        return u'%s' % self.domain