from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from views import *

urlpatterns = patterns('',
    url(r'^$',
        TemplateView.as_view(template_name="phphost/index.html"),
        name="phphost.dashboard.home", ),
    url(r'^dashboard/$',
        dashboard_home_view,
        name="phphost.dashboard.home", ),
    url(r'^dashboard/add/$',
        dashboard_hosting_add_view,
        name="phphost.dashboard.hosting.add", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/$',
        dashboard_hosting_view,
        name="phphost.dashboard.hosting", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/set-ftp-passwd/$',
        set_ftp_passwd_process,
        name="phphost.dashboard.hosting.set_ftp_passwd", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/set-mysql-passwd/$',
        set_mysql_passwd_process,
        name="phphost.dashboard.hosting.set_mysql_passwd", ),
    url(r'^dashboard/(?P<domain>[a-zA-Z0-9-.]+)/create-mysql-db/$',
        create_mysql_db_process,
        name="phphost.dashboard.hosting.create_mysql_db", ),
)