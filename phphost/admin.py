from django.contrib import admin
from models import Hosting, Server


admin.site.register(Hosting)
admin.site.register(Server)
